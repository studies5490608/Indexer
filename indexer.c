#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

typedef struct TermFrequency{
	char *filename;
	int wordAmount;
	int totalWords;
	double termFrequency;
	double tfidf;
} TermFrequency;

void showOptionsMenu();
void freq(int n, char *filename);
int freqWord(char *word, char *filename);
int freqTermInFile(char *word, char *filename, int *totalWords);

void calculateTF(TermFrequency *tf);

//ÁRVORE RED-BLACK OBTIDA DE https://www.programiz.com/dsa/red-black-tree. Adaptada para a situação-problema
enum nodeColor {
  RED,
  BLACK
};

struct rbNode {
  int amount, color;
  char* data;
  struct rbNode *link[2];
};

struct rbNode *root = NULL;

// Create a red-black tree
struct rbNode *createNode(char *data) {
  struct rbNode *newnode;
  newnode = (struct rbNode *)malloc(sizeof(struct rbNode));
  newnode->data = strdup(data);
  newnode->amount = 1;
  newnode->color = RED;
  newnode->link[0] = newnode->link[1] = NULL;
  return newnode;
}

// Insert an node
void insertion(char *data) {
  struct rbNode *stack[98], *ptr, *newnode, *xPtr, *yPtr;
  int dir[98], ht = 0, index;
  ptr = root;
  if (!root) {
    root = createNode(data);
    return;
  }

  stack[ht] = root;
  dir[ht++] = 0;
  while (ptr != NULL) {
    // if (ptr->data == data) {
 	if (strcmp(ptr->data, data) == 0){
      ptr->amount = ptr->amount+1;
      return;
    }
    index = (data - ptr->data) > 0 ? 1 : 0;
    stack[ht] = ptr;
    ptr = ptr->link[index];
    dir[ht++] = index;
  }
  stack[ht - 1]->link[index] = newnode = createNode(data);
  while ((ht >= 3) && (stack[ht - 1]->color == RED)) {
    if (dir[ht - 2] == 0) {
      yPtr = stack[ht - 2]->link[1];
      if (yPtr != NULL && yPtr->color == RED) {
        stack[ht - 2]->color = RED;
        stack[ht - 1]->color = yPtr->color = BLACK;
        ht = ht - 2;
      } else {
        if (dir[ht - 1] == 0) {
          yPtr = stack[ht - 1];
        } else {
          xPtr = stack[ht - 1];
          yPtr = xPtr->link[1];
          xPtr->link[1] = yPtr->link[0];
          yPtr->link[0] = xPtr;
          stack[ht - 2]->link[0] = yPtr;
        }
        xPtr = stack[ht - 2];
        xPtr->color = RED;
        yPtr->color = BLACK;
        xPtr->link[0] = yPtr->link[1];
        yPtr->link[1] = xPtr;
        if (xPtr == root) {
          root = yPtr;
        } else {
          stack[ht - 3]->link[dir[ht - 3]] = yPtr;
        }
        break;
      }
    } else {
      yPtr = stack[ht - 2]->link[0];
      if ((yPtr != NULL) && (yPtr->color == RED)) {
        stack[ht - 2]->color = RED;
        stack[ht - 1]->color = yPtr->color = BLACK;
        ht = ht - 2;
      } else {
        if (dir[ht - 1] == 1) {
          yPtr = stack[ht - 1];
        } else {
          xPtr = stack[ht - 1];
          yPtr = xPtr->link[0];
          xPtr->link[0] = yPtr->link[1];
          yPtr->link[1] = xPtr;
          stack[ht - 2]->link[1] = yPtr;
        }
        xPtr = stack[ht - 2];
        yPtr->color = BLACK;
        xPtr->color = RED;
        xPtr->link[1] = yPtr->link[0];
        yPtr->link[0] = xPtr;
        if (xPtr == root) {
          root = yPtr;
        } else {
          stack[ht - 3]->link[dir[ht - 3]] = yPtr;
        }
        break;
      }
    }
  }
  root->color = BLACK;
}

// Delete a node
// void deletion(char* data) {
//   struct rbNode *stack[98], *ptr, *xPtr, *yPtr;
//   struct rbNode *pPtr, *qPtr, *rPtr;
//   int dir[98], ht = 0, diff, i;
//   enum nodeColor color;
// 
//   if (!root) {
//     printf("Tree not available\n");
//     return;
//   }
// 
//   ptr = root;
//   while (ptr != NULL) {
//     if ((data - ptr->data) == 0)
//       break;
//     diff = (data - ptr->data) > 0 ? 1 : 0;
//     stack[ht] = ptr;
//     dir[ht++] = diff;
//     ptr = ptr->link[diff];
//   }
// 
//   if (ptr->link[1] == NULL) {
//     if ((ptr == root) && (ptr->link[0] == NULL)) {
//       free(ptr);
//       root = NULL;
//     } else if (ptr == root) {
//       root = ptr->link[0];
//       free(ptr);
//     } else {
//       stack[ht - 1]->link[dir[ht - 1]] = ptr->link[0];
//     }
//   } else {
//     xPtr = ptr->link[1];
//     if (xPtr->link[0] == NULL) {
//       xPtr->link[0] = ptr->link[0];
//       color = xPtr->color;
//       xPtr->color = ptr->color;
//       ptr->color = color;
// 
//       if (ptr == root) {
//         root = xPtr;
//       } else {
//         stack[ht - 1]->link[dir[ht - 1]] = xPtr;
//       }
// 
//       dir[ht] = 1;
//       stack[ht++] = xPtr;
//     } else {
//       i = ht++;
//       while (1) {
//         dir[ht] = 0;
//         stack[ht++] = xPtr;
//         yPtr = xPtr->link[0];
//         if (!yPtr->link[0])
//           break;
//         xPtr = yPtr;
//       }
// 
//       dir[i] = 1;
//       stack[i] = yPtr;
//       if (i > 0)
//         stack[i - 1]->link[dir[i - 1]] = yPtr;
// 
//       yPtr->link[0] = ptr->link[0];
// 
//       xPtr->link[0] = yPtr->link[1];
//       yPtr->link[1] = ptr->link[1];
// 
//       if (ptr == root) {
//         root = yPtr;
//       }
// 
//       color = yPtr->color;
//       yPtr->color = ptr->color;
//       ptr->color = color;
//     }
//   }
// 
//   if (ht < 1)
//     return;
// 
//   if (ptr->color == BLACK) {
//     while (1) {
//       pPtr = stack[ht - 1]->link[dir[ht - 1]];
//       if (pPtr && pPtr->color == RED) {
//         pPtr->color = BLACK;
//         break;
//       }
// 
//       if (ht < 2)
//         break;
// 
//       if (dir[ht - 2] == 0) {
//         rPtr = stack[ht - 1]->link[1];
// 
//         if (!rPtr)
//           break;
// 
//         if (rPtr->color == RED) {
//           stack[ht - 1]->color = RED;
//           rPtr->color = BLACK;
//           stack[ht - 1]->link[1] = rPtr->link[0];
//           rPtr->link[0] = stack[ht - 1];
// 
//           if (stack[ht - 1] == root) {
//             root = rPtr;
//           } else {
//             stack[ht - 2]->link[dir[ht - 2]] = rPtr;
//           }
//           dir[ht] = 0;
//           stack[ht] = stack[ht - 1];
//           stack[ht - 1] = rPtr;
//           ht++;
// 
//           rPtr = stack[ht - 1]->link[1];
//         }
// 
//         if ((!rPtr->link[0] || rPtr->link[0]->color == BLACK) &&
//           (!rPtr->link[1] || rPtr->link[1]->color == BLACK)) {
//           rPtr->color = RED;
//         } else {
//           if (!rPtr->link[1] || rPtr->link[1]->color == BLACK) {
//             qPtr = rPtr->link[0];
//             rPtr->color = RED;
//             qPtr->color = BLACK;
//             rPtr->link[0] = qPtr->link[1];
//             qPtr->link[1] = rPtr;
//             rPtr = stack[ht - 1]->link[1] = qPtr;
//           }
//           rPtr->color = stack[ht - 1]->color;
//           stack[ht - 1]->color = BLACK;
//           rPtr->link[1]->color = BLACK;
//           stack[ht - 1]->link[1] = rPtr->link[0];
//           rPtr->link[0] = stack[ht - 1];
//           if (stack[ht - 1] == root) {
//             root = rPtr;
//           } else {
//             stack[ht - 2]->link[dir[ht - 2]] = rPtr;
//           }
//           break;
//         }
//       } else {
//         rPtr = stack[ht - 1]->link[0];
//         if (!rPtr)
//           break;
// 
//         if (rPtr->color == RED) {
//           stack[ht - 1]->color = RED;
//           rPtr->color = BLACK;
//           stack[ht - 1]->link[0] = rPtr->link[1];
//           rPtr->link[1] = stack[ht - 1];
// 
//           if (stack[ht - 1] == root) {
//             root = rPtr;
//           } else {
//             stack[ht - 2]->link[dir[ht - 2]] = rPtr;
//           }
//           dir[ht] = 1;
//           stack[ht] = stack[ht - 1];
//           stack[ht - 1] = rPtr;
//           ht++;
// 
//           rPtr = stack[ht - 1]->link[0];
//         }
//         if ((!rPtr->link[0] || rPtr->link[0]->color == BLACK) &&
//           (!rPtr->link[1] || rPtr->link[1]->color == BLACK)) {
//           rPtr->color = RED;
//         } else {
//           if (!rPtr->link[0] || rPtr->link[0]->color == BLACK) {
//             qPtr = rPtr->link[1];
//             rPtr->color = RED;
//             qPtr->color = BLACK;
//             rPtr->link[1] = qPtr->link[0];
//             qPtr->link[0] = rPtr;
//             rPtr = stack[ht - 1]->link[0] = qPtr;
//           }
//           rPtr->color = stack[ht - 1]->color;
//           stack[ht - 1]->color = BLACK;
//           rPtr->link[0]->color = BLACK;
//           stack[ht - 1]->link[0] = rPtr->link[1];
//           rPtr->link[1] = stack[ht - 1];
//           if (stack[ht - 1] == root) {
//             root = rPtr;
//           } else {
//             stack[ht - 2]->link[dir[ht - 2]] = rPtr;
//           }
//           break;
//         }
//       }
//       ht--;
//     }
//   }
// }

// Print the inorder traversal of the tree
void inorderTraversal(struct rbNode *node) {
  if (node) {
    inorderTraversal(node->link[0]);
    printf("%s (%d)  \n", node->data, node->amount);
    inorderTraversal(node->link[1]);
  }
  return;
}
//FIM DA ÁRVORE RED-BLACK

void greatestAmount(struct rbNode *node, int howMany, struct rbNode *greatestAmounts){
	int i,j;
	struct rbNode auxNode;
	if (node) {
		greatestAmount(node->link[0], howMany, greatestAmounts);
		i=0;
		if(node->amount > greatestAmounts[0].amount){
			while(node->amount > greatestAmounts[i].amount){
				i++;
			}
			for(j=0;j<i-1;j++){
				greatestAmounts[j] = greatestAmounts[j+1];
			}
			greatestAmounts[j] = *node;
		}
		greatestAmount(node->link[1], howMany, greatestAmounts);
	}
}

int main (int argc, char *argv[]){
	setlocale(LC_ALL, "");
	int i, j, convert;
	int total = 0;
	char *command;
	
	if(argc < 2){
		showOptionsMenu();
		return 0;
	}
	
	command = argv[1];
	if(strcmp(command, "--freq") == 0){
		if(argc == 4){
			sscanf(argv[2], "%d", &convert);
			freq(convert, argv[3]);
			
		}else{
			printf("Quantidade de argumentos incorreta\n");
		}
	}else if(strcmp(command, "--freq-word") == 0){
		if(argc == 4){
			i = freqWord(argv[2], argv[3]);
			printf("\"%s\" encontrado %d vezes no arquivo\n", argv[2], i);
			return 0;
		}else{
			printf("Quantidade de argumentos incorreta\n");
		}
	}else if(strcmp(command, "--search") == 0){
		int docsWithTerm = 0, docsAmount = 0, wordAmount = 0;
		double idf = 0;
		TermFrequency frequencies[argc-3];
		TermFrequency aux;
		
		for(i=3;i<argc;i++){
			docsAmount++;
			wordAmount = 0;
			frequencies[i-3].filename = argv[i];
			wordAmount = freqTermInFile(argv[2], argv[i], &total);
			if(wordAmount > 0){
				docsWithTerm++;
				frequencies[i-3].wordAmount = wordAmount;
				frequencies[i-3].totalWords = total;
				frequencies[i-3].termFrequency = (double)frequencies[i-3].wordAmount / frequencies[i-3].totalWords;
			}
		}
		
		idf = (double)log(docsAmount/docsWithTerm);
		for(i=3;i<argc;i++){
			if(idf != 0){
				frequencies[i-3].tfidf = (double)frequencies[i-3].termFrequency*idf;
			}else{
				frequencies[i-3].tfidf = (double)frequencies[i-3].termFrequency;
			}
			// printf("%f\n", frequencies[i-3].tfidf);
			for(j=0;j<i-3;j++){
				if(frequencies[i-3].tfidf > frequencies[j].tfidf){
					aux = frequencies[j];
					frequencies[j] = frequencies[i-3];
					frequencies[i-3] = aux;
				}
			}
		}
		for(i=0;i<argc-3;i++){
			printf("%d - %s: TFIDF = %f\n", i+1, frequencies[i].filename, frequencies[i].tfidf);
		}
		return 0;
	}else if(strcmp(command, "--help") == 0){
		showOptionsMenu();
	}else{
		printf("Comando desconhecido\n");
		showOptionsMenu();
	}
}

void showOptionsMenu(){
	printf("Opções disponíveis:\n");
	printf("--freq N ARQUIVO\nExibe o número de ocorrências das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência\n\tN Quantidade de palavras\n\tARQUIVO Arquivo a ser lido\n");
	printf("--freq-word PALAVRA ARQUIVO\nExibe o número de ocorrências de PALAVRA em ARQUIVO\n\tPALAVRA Palavra a ser encontrada\n\tARQUIVO Arquivo a ser lido\n");
	printf("--search TERMO ARQUIVO [ARQUIVO ...]\nExibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO (para termos com mais de uma palavra, utilize aspas)\n\tTERMO Termo a ser encontrado\n\tARQUIVO Arquivos para pesquisa do termo\n");
}

void freq(int n, char *filename){
	FILE *file;
	char current[1024];
	int i,j;
	struct rbNode greatestAmounts[n];
	
	for(i=0;i<n;i++){
		greatestAmounts[i].amount = 0;
		greatestAmounts[i].data = "";
	}
	
	file = fopen(filename, "r");
	if(file != NULL){
		while(fscanf(file, "%1023s", current) == 1){
			for(i = 0; current[i] != '\0'; ++i){
				current[i] = tolower(current[i]);
				while (!( isalpha(current[i]) || current[i] == '\0') ){
					for(j = i; current[j] != '\0'; ++j){
						current[j] = current[j+1];
					}
					current[j] = '\0'; 
				}
			}
			if(strlen(current) > 2){
				insertion(current);
			}
		}
	}
	fclose(file);
	// inorderTraversal(root);
	greatestAmount(root, n, greatestAmounts);
	for(i=0;i<n;i++){
		printf("%s %d\n", greatestAmounts[i].data, greatestAmounts[i].amount);
	}
}

int freqWord(char *word, char *filename){
	FILE *file;
	int amount = 0;
	char current[1024];
	
	file = fopen(filename, "r");
	if(file != NULL){
		while(fscanf(file, "%1023s", current) == 1){
			if(strlen(current) > 2){
				if(strcmp(current, word) == 0){
					amount++;
				}
			}
		}
	}else{
		printf("Arquivo %s não foi encontrado\n", filename);
	}
	fclose(file);
	return amount;
}

int freqTermInFile(char *word, char *filename, int *totalWords){
	FILE *file;
	int amount = 0;
	char current[1024];
	
	file = fopen(filename, "r");
	if(file != NULL){
		while(fscanf(file, "%1023s", current) == 1){
			if(strlen(current) > 2){
				*totalWords = *totalWords+1;
				if(strcmp(current, word) == 0){
					amount++;
				}
			}
		}
	}else{
		printf("Arquivo %s não foi encontrado\n", filename);
	}
	fclose(file);
	return amount;
}
