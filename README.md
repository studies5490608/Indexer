# Indexer
**Desenvolvido por André Luiz Ribas**
**GRR20195136**

## Instalação

 1. Clone o repositório para sua máquina
 2. Compile o arquivo **_indexer.c_**
 3. Execute o programa compilado
 	- Executar sem parâmetros adicionais mostra os comandos disponíveis

Para executar, certifique-se de que você tenha uma instância de console localizada no diretório em que o programa foi compilado.

`> indexer --comando parâmetros` 

## Parâmetros de execução

 - \-\-freq N ARQUIVO
	 - Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em
 ordem decrescente de ocorrência.
 - \-\-freq-word PALAVRA ARQUIVO
	 - Exibe o número de ocorrências de PALAVRA em ARQUIVO.
 - \-\-search TERMO ARQUIVO [ARQUIVO\.\.\.]
	 - Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por 
 TERMO. A listagem é apresentada em ordem descrescente de relevância. 
 TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre 
 aspas.
